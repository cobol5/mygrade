       IDENTIFICATION DIVISION.
       PROGRAM-ID. READ-MYGRADE.
       AUTHOR. JIRAT.

       ENVIRONMENT DIVISION.  
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  GRADE-FILE.
       01 SCORE-DETAILS.
          88 END-OF-SCORE-FILE               VALUE HIGH-VALUE.
       05 SUBJECT-ID.
          10 TWO-FRONT          PIC 9(2).
          10 TWO-MIDDLE         PIC 9(2).
          10 TWO-REAR           PIC 9(2).
       05 SUBJECT-NAME          PIC X(50).
          05 SUBJECT-UNIT       PIC 9(1).
          05 SUBJECT-GRADE      PIC X(2).
       
       FD  AVG-GRADE-FILE.
       01 AVG-GRADE-DETAIL.
          05 AVG-NAME           PIC X(20).
          05 AVG-GRADE          PIC 9.999.
       01 UNIT-DETAIL.
          05 UNIT-NAME          PIC X(20).
          05 TOTAL-UNIT         PIC 9(3).
     
       WORKING-STORAGE SECTION. 
       01 COUNT-SUBJECT         PIC 9(2)     VALUE ZEROS.
       01 COUNT-SCI-SUBJECT     PIC 9(2)     VALUE ZEROS.
       01 COUNT-CS-SUBJECT      PIC 9(2)     VALUE ZEROS.

       01 SUM-UNIT              PIC 9(3).
       01 SUM-UNIT-SCI          PIC 9(3).
       01 SUM-UNIT-CS           PIC 9(3).

       01 CON-GRADE             PIC 9(2)V999.

       01 SUM-UNIT-GRADE        PIC 9(3)V999.
       01 SUM-UNIT-GRADE-SCI    PIC 9(3)V999.
       01 SUM-UNIT-GRADE-CS     PIC 9(3)V999.

       01 AVG-GPA               PIC 9(1)V999.
       01 AVG-GPA-SCI           PIC 9(1)V999.
       01 AVG-GPA-CS            PIC 9(1)V999.
       
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE
           PERFORM UNTIL END-OF-SCORE-FILE  
                   READ GRADE-FILE
                   AT END
                      SET END-OF-SCORE-FILE TO TRUE 
                   END-READ 
                   IF NOT END-OF-SCORE-FILE THEN
                      PERFORM 001-PROCESS-SHOW-ALL THRU 001-EXIT 
                      PERFORM 002-PROCESS-SUM-UNIT THRU 002-EXIT
                      PERFORM 003-PROCESS-CON-GRADE THRU 003-EXIT
                      PERFORM 004-PROCESS-CAL-AVG-GRADE THRU 004-EXIT
                   END-IF 
              
           END-PERFORM
           PERFORM 006-PROCESS-SHOW-DETAIL THRU 006-EXIT
           CLOSE GRADE-FILE
           PERFORM 005-WRITE-FILE THRU 005-EXIT
           GOBACK 
           .         
       
       001-PROCESS-SHOW-ALL.
           DISPLAY "==================================================" 
           DISPLAY "SUBJECT-ID: " SUBJECT-ID    
           DISPLAY "NAME: " SUBJECT-NAME 
           DISPLAY "UNIT: " SUBJECT-UNIT  
           DISPLAY "GRADE: " SUBJECT-GRADE 
           .

       001-EXIT.
           EXIT.

       002-PROCESS-SUM-UNIT.
           COMPUTE SUM-UNIT = SUM-UNIT + SUBJECT-UNIT 
           COMPUTE COUNT-SUBJECT = COUNT-SUBJECT + 1  

           IF TWO-FRONT IN SUBJECT-ID(1:1) IS EQUAL TO "3" THEN
              COMPUTE SUM-UNIT-SCI = SUM-UNIT-SCI + SUBJECT-UNIT 
              COMPUTE COUNT-SCI-SUBJECT = COUNT-SCI-SUBJECT + 1 
           END-IF 

           IF TWO-FRONT IN SUBJECT-ID IS EQUAL TO "31" THEN
              COMPUTE SUM-UNIT-CS = SUM-UNIT-CS + SUBJECT-UNIT
              COMPUTE COUNT-CS-SUBJECT = COUNT-CS-SUBJECT + 1  
           END-IF
           .

       002-EXIT.
           EXIT.
       
       003-PROCESS-CON-GRADE.
           EVALUATE TRUE
           WHEN SUBJECT-GRADE = "A"
                MOVE 4.0 TO CON-GRADE 
           WHEN SUBJECT-GRADE = "B+"
                MOVE 3.5 TO CON-GRADE
           WHEN SUBJECT-GRADE = "B"
                MOVE 3.0 TO CON-GRADE
           WHEN SUBJECT-GRADE = "C+"
                MOVE 2.5 TO CON-GRADE
           WHEN SUBJECT-GRADE = "C"
                MOVE 2.0 TO CON-GRADE
           WHEN SUBJECT-GRADE = "D+"
                MOVE 1.5 TO CON-GRADE
           WHEN SUBJECT-GRADE = "D"
                MOVE 1.0 TO CON-GRADE
           END-EVALUATE
           .

       003-EXIT.
           EXIT.

       004-PROCESS-CAL-AVG-GRADE.
           PERFORM 003-PROCESS-CON-GRADE THRU 003-EXIT 
           
           COMPUTE SUM-UNIT-GRADE
              =(SUBJECT-UNIT * CON-GRADE) + SUM-UNIT-GRADE 
           COMPUTE AVG-GPA = SUM-UNIT-GRADE / SUM-UNIT

           IF TWO-FRONT IN SUBJECT-ID(1:1) IS EQUAL TO "3" THEN
              COMPUTE SUM-UNIT-GRADE-SCI
                 =(SUBJECT-UNIT * CON-GRADE) + SUM-UNIT-GRADE-SCI 
              COMPUTE AVG-GPA-SCI = SUM-UNIT-GRADE-SCI / SUM-UNIT-SCI
           END-IF 

           IF TWO-FRONT IN SUBJECT-ID IS EQUAL TO "31" THEN
              COMPUTE SUM-UNIT-GRADE-CS
                 =(SUBJECT-UNIT * CON-GRADE) + SUM-UNIT-GRADE-CS
              COMPUTE AVG-GPA-CS = SUM-UNIT-GRADE-CS / SUM-UNIT-CS
           END-IF 

           .

       004-EXIT.
           EXIT.

       005-WRITE-FILE.
           OPEN OUTPUT AVG-GRADE-FILE 

           MOVE "AVG-GRADE: " TO AVG-NAME
           MOVE AVG-GPA TO AVG-GRADE
           WRITE AVG-GRADE-DETAIL 

           MOVE "AVG-SCI-GRADE: " TO AVG-NAME
           MOVE AVG-GPA-SCI TO AVG-GRADE  
           WRITE AVG-GRADE-DETAIL

           MOVE "AVG-CS-GRADE: " TO AVG-NAME
           MOVE AVG-GPA-CS TO AVG-GRADE 
           WRITE AVG-GRADE-DETAIL 

           MOVE "UNIT-ALL: " TO UNIT-NAME  
           MOVE SUM-UNIT TO TOTAL-UNIT 
           WRITE UNIT-DETAIL 

           MOVE "UNIT-SCI: " TO UNIT-NAME  
           MOVE SUM-UNIT-SCI  TO TOTAL-UNIT 
           WRITE UNIT-DETAIL

           MOVE "UNIT-CS: " TO UNIT-NAME  
           MOVE SUM-UNIT-CS  TO TOTAL-UNIT 
           WRITE UNIT-DETAIL 

           CLOSE AVG-GRADE-FILE 
           .

       005-EXIT.
           EXIT.

       006-PROCESS-SHOW-DETAIL.
           DISPLAY " "
           DISPLAY "ALl SUBJECT: " COUNT-SUBJECT
           DISPLAY "SCI SUBJECT: " COUNT-SCI-SUBJECT
           DISPLAY "CS SUBJECT: " COUNT-CS-SUBJECT
           DISPLAY " "
           DISPLAY "ALL UNIT: " SUM-UNIT 
           DISPLAY "SCI UNIT: " SUM-UNIT-SCI
           DISPLAY "CS UNIT: " SUM-UNIT-CS
           DISPLAY " "
           DISPLAY "SUM: "
                   SUM-UNIT-GRADE
                   ", "
                   SUM-UNIT-GRADE-SCI
                   ", "
                   SUM-UNIT-GRADE-CS
           DISPLAY " "
           DISPLAY "AVG-GRADE: " AVG-GPA
           DISPLAY "AVG-GRADE-SCI: " AVG-GPA-SCI
           DISPLAY "AVG-GRADE-CS: " AVG-GPA-CS
           .

       006-EXIT.
           EXIT.